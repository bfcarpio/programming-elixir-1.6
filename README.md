# Programming Elixir 1.6

Files and notes from my reading of The Pragmatic Programmers' book "Programming Elixir >= 1.6"

## Chapter 2: Pattern Matching

```elixir
a = 2
^a = 2 # Pin asserts the last assigned value. Here is same as 2 = 2
^a = 1 # Doesn't work because it's 2 = 1
^a = 2 - a # Doesn't work: 2 = 2 - 2
```

## Chapter 3: Immutability

## Chapter 4: Elixir Basics
+ Generally variables will be scoped through the entire function that it's in. Using `with` allows making small temporary scope where you can have temporary variables. The given example is using `with` in a function to load a file, use it's contents in a string interpolation, then assign the string to a variable. This way you don't have the file loading variables also in full scope.
+ Using `<-` will allow returning the unmatched value.

```elixir
with [a|_] <- [1,2,3], do: a
# 1
with [a|_] <- nil, do: a
#nil
```
