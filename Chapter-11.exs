defmodule C11 do
  def valid_ascii?([]), do: true
  def valid_ascii?([h | tail])
    when h >= 32 and h < 127,
    do: valid_ascii?(tail)

  def valid_ascii?(_), do: false

  def anagram?(a, b) do
    Enum.sort(a) == Enum.sort(b)
  end
end


ExUnit.start()
ExUnit.configure(exclude: :pending, trace: true)

defmodule Chapter11Exercises do
  use ExUnit.Case, async: true

  # Strings and Binaries 1
  test "Valid Ascii" do
    assert C11.valid_ascii?('abc')
    refute C11.valid_ascii?([65, 66, 67, 0])
  end

  # Strings and Binaries 2
  test "Anagram" do
    assert C11.anagram?('resistance', 'ancestries')
    refute C11.anagram?('abc', 'xyz')
  end

end
