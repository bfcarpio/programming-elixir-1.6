ExUnit.start()
ExUnit.configure(exclude: :pending, trace: true)

defmodule Chapter5Exercises do
  use ExUnit.Case, async: true

  # Functions 1
  test "List concatenation" do
    list_concat = fn x, y -> x ++ y end

    assert list_concat.([:a, :b], [:c, :d]) == [:a, :b, :c, :d]
  end

  test "Sum" do
    sum = fn a, b, c -> a + b + c end

    assert sum.(1, 2, 3) == 6
  end

  test "Tuple to list" do
    pair_tuple_to_list = fn {a, b} -> [a, b] end

    assert pair_tuple_to_list.({1234, 5678}) == [1234, 5678]
  end

  # Functions 2 & 3
  test "Fizzbuzz" do
    # Trying not to use things not yet gone over in the book
    fizzbuzz = fn
      0, 0, _ -> "FizzBuzz"
      0, _, _ -> "Fizz"
      _, 0, _ -> "Buzz"
      _, _, x -> x
    end

    assert fizzbuzz.(0, 0, 123) == "FizzBuzz"
    assert fizzbuzz.(0, 123, 123) == "Fizz"
    assert fizzbuzz.(123, 0, 123) == "Buzz"
    assert fizzbuzz.(123, 456, 789) == 789

    10..16
    |> Enum.map(fn n -> fizzbuzz.(rem(n,3), rem(n,5), n) end)
    |> Enum.reduce("", fn x, acc -> acc <> "#{x}, " end)
    |> String.slice(0..-3)
    |> (fn x -> assert x == "Buzz, 11, Fizz, 13, 14, FizzBuzz, 16" end).()
  end

  # Functions 4
  test "prefix" do
    prefix = fn prefix -> (fn name -> prefix <> " " <> name end) end

    assert prefix.("Mrs.").("Smith") == "Mrs. Smith"
    assert prefix.("Elixir").("Rocks") == "Elixir Rocks"
  end

  # Functions 5
  test "& functions" do
    Enum.map([1, 2, 3, 4], &(&1 + 2))
    # Enum.map([1, 2, 3, 4], &IO.inspect/1)
  end

end