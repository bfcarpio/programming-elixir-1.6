defmodule C7 do
  def mapsum([], _func), do: 0
  def mapsum([ h | tail ], func) do
    func.(h) + mapsum(tail, func) 
  end

  def max([x]), do: x
  def max([ h | tail ]), do: Kernel.max(h, max(tail))

  def caesar([], _offset), do: []
  def caesar([h | tail], offset) do
    [rem(h + offset - 97, 26) + 97 | caesar(tail, offset)]
  end

  def span(from, from), do: []
  def span(from, to) when from > to, do: []
  def span(from, to) when from < to do
    [from | span(from + 1, to)]
  end
end


ExUnit.start()
ExUnit.configure(exclude: :pending, trace: true)

defmodule Chapter7Exercises do
  use ExUnit.Case, async: true

  # Lists and Recursions 1
  test "Mapsum" do
    assert C7.mapsum([1, 2, 3], &(&1 * &1)) == 14
  end

  # Lists and Recursions 2
  test "Max" do
    assert C7.max([1, 2, 3]) == 3
  end

  # Lists and Recursions 3
  test "Caesar" do
    assert C7.caesar('ryvkve', 13) == 'elixir'
  end

  test "Span (range)" do
    assert C7.span(0, 3) == [0, 1, 2]
  end

end