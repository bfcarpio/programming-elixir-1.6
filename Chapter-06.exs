defmodule C6 do
  def double(n), do: n * 2
  def triple(n), do: n * 3
  def quadruple(n), do: double(n) |> double()
  def sum(0), do: 0
  def sum(n), do: n + sum(n-1)
  def gcd(x, 0), do: x
  def gcd(x, y), do: gcd(y, rem(x,y))
  def guess(ans, range = a..b) do
    guess = div(a + b, 2)
    IO.puts "Is it #{guess}?"
    _guess ans, guess, range
  end

  defp _guess(ans, ans, _) do
    IO.puts "Yes, it's #{ans}!"
    ans
  end

  defp _guess(ans, guess, _a..b)
    when guess < ans,
    do: guess(ans, guess+1..b)

  defp _guess(ans, guess, a.._b)
    when guess > ans,
    do: guess(ans, a..guess-1)

end


ExUnit.start()
ExUnit.configure(exclude: :pending, trace: true)

defmodule Chapter6Exercises do
  use ExUnit.Case, async: true

  # Functions 1 & 2
  test "Triple" do
    assert C6.triple(2) == 6
  end

  # Functions 3
  test "Quadruple" do
    assert C6.quadruple(2) == 8
  end

  # Functions 4
  test "Sum" do
    assert C6.sum(3) == 6
  end

  # Functions 5
  test "GCD" do
    assert C6.gcd(30, 15) == 15
    assert C6.gcd(7, 9) == 1
  end

  # Functions 6
  test "Guess" do
    assert C6.guess(273, 1..1000) == 273
  end

  # Functions 7
  # :io.format()
  # System
  # Path
  # Path
  # Path
  # poison or elixir-json
  # System

end