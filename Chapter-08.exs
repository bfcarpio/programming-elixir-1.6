defmodule C8 do
  def add_total_amount(orders, rates) do
    Enum.map(orders, &(calc_sales_tax(&1, rates)))
  end

  defp calc_sales_tax(order = [id: _, ship_to: state, net_amount: amt], rates) do
    tax_rate = Keyword.get(rates, state, 0)
    total = amt * (1.0 + tax_rate)
    Keyword.put(order, :total_amount, total)
  end
end


# ExUnit.start()
# ExUnit.configure(exclude: :pending, trace: true)

# defmodule Chapter8Exercises do
#   use ExUnit.Case, async: true

#   # Lists and Recursions 7
#   test "Primes" do
#   end

#   # List and Recursions 8
#   test "Sales Tax" do

#   end

end
